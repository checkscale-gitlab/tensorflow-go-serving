package main

import (
	"fmt"
	"html/template"
	"net/http"
	"time"
)

type Data struct {
	Name ,Time string
	
}

func main() {
	data := Data{"moti", time.Now().Format(time.Stamp)}
	templates := template.Must(template.ParseFiles("app/template/welcome.html"))

	http.Handle("/app/static/", //final url can be anything
		http.StripPrefix("/app/static/",
			http.FileServer(http.Dir("app/static"))))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if name := r.FormValue("name"); name != "" {
			data.Name = name
		}
		//If errors show an internal server error message
		//I also pass the 'data' struct to the welcome.html file.
		if err := templates.ExecuteTemplate(w, "welcome.html", data); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	fmt.Println("Listening on localhost:8080")
	fmt.Println(http.ListenAndServe(":8080", nil))

}
